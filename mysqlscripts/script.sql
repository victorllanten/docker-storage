
CREATE DATABASE example1;

CREATE TABLE example1.persons (
	id INT auto_increment primary key NOT NULL,
	rut varchar(100) NOT NULL,
	name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	UNIQUE KEY persons_rut_IDX (rut) USING BTREE
);

INSERT INTO example1.persons(rut, name, last_name)
VALUES('16310114-9', 'Victor Hugo', 'Llanten Galaz'),
('11953617-0', 'Alejandro Alberto', 'Landa Melo');